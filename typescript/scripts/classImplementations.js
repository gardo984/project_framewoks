var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var InventoryItem = /** @class */ (function () {
    function InventoryItem(item) {
        this.name = item.name;
        this.price = item.price;
    }
    return InventoryItem;
}());
var Product = /** @class */ (function (_super) {
    __extends(Product, _super);
    function Product(item) {
        var _this = _super.call(this, item) || this;
        _this.color = "green";
        return _this;
    }
    Product.prototype.printColor = function () {
        console.log(this.color);
        return this;
    };
    Product.prototype.subjectMessage = function () {
        var defaultMsg = "Default Subject";
        return defaultMsg;
    };
    Product.prototype.bodyMessage = function () {
        var defaultBody = "Default Body Content";
        return defaultBody;
    };
    return Product;
}(InventoryItem));
function sendEmail(item, to) {
    console.log("Sending email to ".concat(to));
    console.log("email subject: ".concat(item.subjectMessage()));
    console.log("Sending email: ".concat(item.bodyMessage()));
}
var item = new Product({ name: "bimbo", price: 20 });
sendEmail(item, 'admin@mailinator.com');
