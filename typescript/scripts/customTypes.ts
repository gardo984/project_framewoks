
type User = {
	name: string,
	lastname: string,
	age: number,
}

function printUser(user: User) {
	console.log(user)
}

printUser({ name: "manuel", lastname: "lazo", age: 32 })

interface InventoryItem {
	name: string,
	price: number,
}

interface Category {
	name: string,
}

interface Product extends InventoryItem {
	category: Category,
}
interface Service extends InventoryItem {
	field: string
}

function printResource(resource: Product | Service) {
	if (resource.hasOwnProperty("product")) {
		let categoryName: string = resource["category"].name
		console.log(`it is a product ${resource.name} of category ${categoryName}`)
	} else {
		let serviceField: string = resource["field"]
		console.log(`it is a service ${resource.name} that works in the field ${serviceField}`)
	}
}

enum Colors {
	green = "Green",
	read = "Red",
	blue = "Blue",
}

function printColor(color: Colors) {}
printColor(Colors.green)
