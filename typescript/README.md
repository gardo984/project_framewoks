
# Typescript Fundamentals

Document will contain good practices and keywords related to typescript language program.

Glossary:

- [Type Annotations](#type-annotations)
	- [Basic Types](#basic-types)
	- [Declaration Types](#declaration-types)
	- [Annotations on function Arguments](#annotations-on-function-arguments)
	- [Custom Types](#custom-types)
	- [Classes](#classes)
	- [Type Assertions](#type-assertions)
	- [Generics](#generics)
	- [Type Guards](#type-guards)
- [References](#references)


## Type Annotations

### Basic Types

The type annotations allowed:

- string
- number
- boolean
- date
- string[]
- number[]
- Union Types (**`white list of types for a variable`**):
```typescript
function printColor(myId: string | number) {
	if (typeof myId == "string"){
		console.log(myId.toUpperCase())
	} else {
		console.log(myId)
	}	
}
```
- Literal Values (**`white list of allowed values`**):
```typescript
function printColor(color: 'white' | 'red' | 'blue') {
	console.log(color)
}
```

### Declaration Types
There are 2 ways to define a variable, `Implicit` and `Explicit`:
- Implicit:
```typescript
let myNumber = 20
```
- Explicit:
```typescript
let myNumber: number = 20
```

### Annotations on function Arguments

- You can specify parameter's type and also the function return value type, such the following:
```typescript
function sum(a: number, b: number): number {
	return a + b	
}
```
- Optional parameters (take a look into the character **`?`** after the variable name):
```typescript
function greetings(myName?: string) {
	if (myName){
		console.log(`hello ${myName}`)
	} else {
		console.log(`hello stranger`)
	}
}
```

### Custom Types

To work with objects creating types or interfaces comes handy, such as:

- defining **`types`**:
```typescript
type User = {
	name: string,
	lastname: string,
	age: number,
}
function printUser(user: User) {
	console.log(user)
}
let exampleUser = {
	name: "jose",
	lastname: "alvarez",
	age: 30,
}
printUser(exampleUser)
```
- Defining **`interfaces`**, one difference is that `interfaces` allows you to:
	- Add additional fields (redifining the interface).
	- define function fields.
	- Inheritance.
	```typescript
	interface InventoryItem {
		name: string,
		price: number,
	}

	interface Category {
		name: string,
	}

	interface Product extends InventoryItem {
		category: Category,
	}
	interface Service extends InventoryItem {
		field: string
	}

	function printResource(resource: Product | Service) {
		if (resource.hasOwnProperty("product")) {
			let categoryName: string = resource["category"].name
			console.log(`it is a product ${resource.name} of category ${categoryName}`)
		} else {
			let serviceField: string = resource["field"]
			console.log(`it is a service ${resource.name} that works in the field ${serviceField}`)
		}
	}
	```
- Defining **`enums`**:
```typescript
enum Colors {
	green = "Green",
	read = "Red",
	blue = "Blue",
}

function printColor(color: Colors) {}
printColor(Colors.green)
```

### Classes

- Defining a class:
```typescript
enum Sizes {
	small = "Small",
	large = "Large",
	medium = "Medium",
}
class Product {
	name: string
	price: number
	color: string = 'green'
	size?: Sizes

	constructor(name: string, price: number) {
		this.name = name;
		this.price = price;
	}
	buy(): this {
		console.log(this.price)
		return this
	}
}

const product = new Product("Bimbo", 20)
product.color = "red"
product.size = Sizes.medium
product.buy()
```
- Class inheritance:
```typescript
enum Sizes {
	small = "Small",
	large = "Large",
	medium = "Medium",
}

type InventoryItemType =  {
	name: string,
	price: number,
}

class InventoryItem {
	name: string
	price: number

	constructor(item: InventoryItemType) {
		this.name = item.name
		this.price = item.price
	}

	buy(): this {
		console.log(this.price)
		return this
	}
}

class Product extends InventoryItem {
	color: string = "green"
	size? : Sizes
	constructor(item: InventoryItemType) {
		super(item)
	}
}

const product = new Product({name: "bimbo", price: 20})
product.size = Sizes.small
product.color = "red"
product.buy()
```
- Class Implementations from `interfaces`:
```typescript

type InventoryItemType = {
	name: string,
	price: number,
}

class InventoryItem {
	name: string
	price: number

	constructor(item: InventoryItemType) {
		this.name = item.name
		this.price = item.price
	}
}

interface Emailable {
	subjectMessage(): string,
	bodyMessage(): string,
}

class Product extends InventoryItem implements Emailable {
	color?: string = "green"

	constructor(item: InventoryItemType) {
		super(item)
	}
	printColor(): this {
		console.log(this.color)
		return this
	}
	subjectMessage(): string {
		let defaultMsg: string = "Default Subject"
		return defaultMsg
	}
	bodyMessage(): string {
		let defaultBody: string = "Default Body Content"
		return defaultBody
	}
}

function sendEmail(item: Emailable, to: string) {
	console.log(`Sending email to ${to}`)
	console.log(`email subject ${item.subjectMessage()}`)
	console.log(`Sending email to ${item.bodyMessage()}`)
}

const item = new Product({ name: "bimbo", price: 20})
sendEmail(item, 'admin@mailinator.com')
```
- Class access modifiers:
	- **`private`**: available only in the defined class.
	- **`protected`**: available on the childs environment, not public.
	- **`public`**: available in all environments.
	- **`readonly`**: available in childs but just readonly.
	```typescript
	type InventoryItemType = {
		name: string,
		price: number,
		category?: string,
	}

	class InventoryItem {
		name: string
		price: number
		private hashId: string
		readonly warehouse: string = "Small Shoe SAC"
		protected category?: string = "-"
		constructor(item: InventoryItemType) {
			this.name = item.name
			this.price = item.price
			this.category = item.category
			this.hashId = new Date().getTime().toString()
		}
	}

	class Product extends InventoryItem {
		color?: string = "Green"

		setCategory(value: string): void {
			this.category = value
		}
		printCategory():void {
			console.log(`Category: ${this.category}`)
		}
	}

	const item: Product = new Product({name: "Bimbo", price: 20, category: "Cakes"})
	item.printCategory()
	item.setCategory("Candies")
	item.printCategory()
	```
### Type Assertions

```typescript
function getPrice(value: number, format: boolean = false): string | number {
	return format ? value.toString().concat(" USD"): value
}

let myString = <string> getPrice(20, true)
let myNumber = <number> getPrice(20, false)
console.log(myString)
console.log(myNumber)
```

### Generics

```typescript
interface Person {
	name: string,
	age: number,
}
const cloneObject = <T> (item: T): T => {
	let newObj = JSON.stringify(item)
	return JSON.parse(newObj)
}

const item: Person = {name: "Jose", age: 20}
const newClone = cloneObject(item)
console.log(newClone.name)
```

### Type Guards

- To check if a variable is a instance of a Class:
```
if (item instanceof Person) {
	...
}
```
- To check if a object property exists:
```
if ('name' in item) {
	...
}
if (item.hasOwnProperty('name')) {
	...
}
```

## References
- https://github.com/microsoft/TypeScript-Sublime-Plugin
- https://www.typescriptlang.org/docs/handbook/2/generics.html#generic-types
- https://www.typescriptlang.org/tsconfig
- https://www.typescriptlang.org/docs/handbook/utility-types.html