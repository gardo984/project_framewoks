var Sizes;
(function (Sizes) {
    Sizes["small"] = "Small";
    Sizes["large"] = "Large";
    Sizes["medium"] = "Medium";
})(Sizes || (Sizes = {}));
var Product = /** @class */ (function () {
    function Product(name, price) {
        this.color = 'green';
        this.name = name;
        this.price = price;
    }
    Product.prototype.buy = function () {
        console.log(this.price);
        return this;
    };
    return Product;
}());
var product = new Product("Bimbo", 20);
product.color = "red";
product.size = Sizes.medium;
product.buy();
