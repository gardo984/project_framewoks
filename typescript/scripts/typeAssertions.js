function getPrice(value, format) {
    if (format === void 0) { format = false; }
    return format ? value.toString().concat(" USD") : value;
}
var myString = getPrice(20, true);
var myNumber = getPrice(20, false);
console.log(myString);
console.log(myNumber);
var cloneObject = function (item) {
    var newObj = JSON.stringify(item);
    return JSON.parse(newObj);
};
var item = { name: "Jose", age: 20 };
var newClone = cloneObject(item);
console.log(newClone.name);
