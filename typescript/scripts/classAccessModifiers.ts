
type InventoryItemType = {
	name: string,
	price: number,
	category?: string,
}

class InventoryItem {
	name: string
	price: number
	private hashId: string
	readonly warehouse: string = "Small Shoe SAC"
	protected category?: string = "-"
	constructor(item: InventoryItemType) {
		this.name = item.name
		this.price = item.price
		this.category = item.category
		this.hashId = new Date().getTime().toString()
	}
}

class Product extends InventoryItem {
	color?: string = "Green"

	setCategory(value: string): void {
		this.category = value
	}
	printCategory():void {
		console.log(`Category: ${this.category}`)
	}
}

const item: Product = new Product({name: "Bimbo", price: 20, category: "Cakes"})
item.printCategory()
item.setCategory("Candies")
item.printCategory()

