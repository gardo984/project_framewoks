
type InventoryItemType = {
	name: string,
	price: number,
}

class InventoryItem {
	name: string
	price: number

	constructor(item: InventoryItemType) {
		this.name = item.name
		this.price = item.price
	}
}

interface Emailable {
	subjectMessage(): string,
	bodyMessage(): string,
}

class Product extends InventoryItem implements Emailable {
	color?: string = "green"

	constructor(item: InventoryItemType) {
		super(item)
	}
	printColor(): this {
		console.log(this.color)
		return this
	}
	subjectMessage(): string {
		let defaultMsg: string = "Default Subject"
		return defaultMsg
	}
	bodyMessage(): string {
		let defaultBody: string = "Default Body Content"
		return defaultBody
	}
}

function sendEmail(item: Emailable, to: string) {
	console.log(`Sending email to ${to}`)
	console.log(`email subject: ${item.subjectMessage()}`)
	console.log(`Sending email: ${item.bodyMessage()}`)
}

const item = new Product({ name: "bimbo", price: 20})
sendEmail(item, 'admin@mailinator.com')
  