enum Sizes {
	small = "Small",
	large = "Large",
	medium = "Medium",
}

type InventoryItemType =  {
	name: string,
	price: number,
}

class InventoryItem {
	name: string
	price: number

	constructor(item: InventoryItemType) {
		this.name = item.name
		this.price = item.price
	}

	buy(): this {
		console.log(this.price)
		return this
	}
}

class Product extends InventoryItem {
	color: string = "green"
	size? : Sizes
	constructor(item: InventoryItemType) {
		super(item)
	}
}

const product = new Product({name: "bimbo", price: 20})
product.size = Sizes.small
product.color = "red"
product.buy()