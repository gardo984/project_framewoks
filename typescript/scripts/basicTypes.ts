let myNumber: number = 2
let myBoolean: boolean = false

let myBooks: string[] = [
	"Peacefull warrior way",
	"the stranger",
	"the warrior light"
]

console.log(myNumber)
console.log(myBoolean)
console.log(myBooks)

// Type Annotations
function sum(a: number, b: number): number {
	return a + b
}

// Optional Parameters
function greetings(myName?: string) {
	if (myName) {
		console.log(`Hello ${myName}`)
	} else {
		console.log(`Hello stranger`)
	}
}

// Union Types
function log(myId: number | string) {
	if (typeof myId == "string") {
		console.log(myId.toUpperCase())
	} else {
		console.log(myId)
	}
}

// Literal Types
function printColor(color: 'white' | 'red' | 'blue') {
	console.log(color)
}

// Defining Types

type allowedColors = "white" | "red" | "blue";
function printAllowedColor( color: allowedColors) {
	console.log(color)	
}
