var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (Object.prototype.hasOwnProperty.call(b, p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        if (typeof b !== "function" && b !== null)
            throw new TypeError("Class extends value " + String(b) + " is not a constructor or null");
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var InventoryItem = /** @class */ (function () {
    function InventoryItem(item) {
        this.warehouse = "Small Shoe SAC";
        this.category = "-";
        this.name = item.name;
        this.price = item.price;
        this.category = item.category;
        this.hashId = new Date().getTime().toString();
    }
    return InventoryItem;
}());
var Product = /** @class */ (function (_super) {
    __extends(Product, _super);
    function Product() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.color = "Green";
        return _this;
    }
    Product.prototype.setCategory = function (value) {
        this.category = value;
    };
    Product.prototype.printCategory = function () {
        console.log("Category: ".concat(this.category));
    };
    Product.prototype.printHashId = function () {
        console.log("Id: ".concat(this.hashId));
    };
    return Product;
}(InventoryItem));
var item = new Product({ name: "Bimbo", price: 20, category: "Cakes" });
item.printCategory();
item.setCategory("Candies");
item.printCategory();
item.printHashId();
