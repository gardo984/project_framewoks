
function getPrice(value: number, format: boolean = false): string | number {
	return format ? value.toString().concat(" USD"): value
}

let myString = <string> getPrice(20, true)
let myNumber = <number> getPrice(20, false)
console.log(myString)
console.log(myNumber)


interface Person {
	name: string,
	age: number,
}
const cloneObject = <T> (item: T): T => {
	let newObj = JSON.stringify(item)
	return JSON.parse(newObj)
}

const item: Person = {name: "Jose", age: 20}
const newClone = cloneObject(item)
console.log(newClone.name)

