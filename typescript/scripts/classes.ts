
enum Sizes {
	small = "Small",
	large = "Large",
	medium = "Medium",
}
class Product {
	name: string
	price: number
	color: string = 'green'
	size?: Sizes

	constructor(name: string, price: number) {
		this.name = name;
		this.price = price;
	}
	buy(): this {
		console.log(this.price)
		return this
	}
}

const product = new Product("Bimbo", 20)
product.color = "red"
product.size = Sizes.medium
product.buy()